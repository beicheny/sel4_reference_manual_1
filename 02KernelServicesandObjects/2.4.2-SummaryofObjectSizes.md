### 2.4.2  对象大小概览

在重新分配内存时，了解对象将占用多少内存是很有用的。对象大小在libsel4中定义。

注意，CNodes、SC和未分配内存对象都是可变大小的。当把未分配内存重新分配成CNodes、SC，或将其分成更小的未分配内存对象时，seL4_Untyped_Retype()的size_bits参数用于指定生成对象的大小。对于所有其他对象类型，大小是固定的，seL4_Untyped_Retype()的size_bits参数将被忽略。

类型    |   size_bits含义[^1] | 字节大小
------- | --------------- | --------
CNode   | log2(slots数量) | 2^size_bits x 2^seL4_SlotBits<br>seL4_SlotBits：<br>32位架构：4<br>64位架构：5
SchedContext(仅MCS) | log2(字节数量)  | 2^size_bits 
Untyped | log2(字节数量)  | 2^size_bits

表格2.1：可变大小类型对象size_bits的含义

对seL4_Untyped_Retype()的单个调用可以将一个未分配内存对象重新分配为多个对象，要创建的对象数量由num_objects参数指定。同时创建的所有对象必须具有相同的类型，由type参数指定。对于可变大小的对象，每个对象的大小也必须相同。如果所需的内存区域大小(通过对象大小乘以num_objects计算)大于未分配内存对象剩余内存，则会导致错误。

[^1]: seL4中凡是2的整数幂大小，基本都是用位数，即幂、指数、log2的对数，来表示，在源码中后缀Bits
