#### 10.2.4.9  DumpAllThreadsUtilisation

LIBSEL4_INLINE_FUNC void seL4_BenchmarkDumpAllThreadsUtilisation

打印当前CPU节点每个线程的累计周期计数。

类型 | 名字 | 描述
--- | --- | ---
void |  | 

*返回值*：无。

*描述*：用内核打印函数按行输出各线程的周期计数，格式：线程名,线程周期数
